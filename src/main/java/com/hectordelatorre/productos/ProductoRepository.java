package com.hectordelatorre.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface ProductoRepository extends MongoRepository<ProductoModel,String> {


}
